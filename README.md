# soal-shift-sisop-modul-1-C03-2022



## Anggota Kelompok



```
Nama    : Ida Bagus Kade Rainata Putra Wibawa
Nrp     : 5025201235

Nama    : Moh. Ilham Fakhri Zamzami
Nrp     : 5025201275

Nama    : Ichsanul Aulia
Nrp     : 05111840007001
```
<div style="text-align: justify">

## Nomer 1

### 1A
- Membuat file register.sh dan setiap user yang berhasil mendaftar, username dan passwordnya disimpan dalam file user.txt. Kodenya:

```
    echo "$username $password" >> user.txt
```

### 1B
- Input password harus hidden ketika login. Kodenya:

```
    read -s -p "Enter password: " password
```
- Minimal password harus memiliki 8 karakter. Cup dengan menghitung jumlah karakter pada password saja.
```
    len=${#password}
    ........
    elif [ $len -lt 8 ]
    then
        echo "password harus lebih dari 8 karakter"
```
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil.  
Disini kami membuat 2 string yang berisi seluruh huruf kecil dan huruf besar, lalu melakukan iterasi pada password apakah password mengandung setidaknya satu dari karakter tersebut. Kami menggunakan fungsi untuk hal ini
```
    not_Big () {
    cap="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    small="abcdefghijklmnopqrstuvwxyz"
    pass=$1
    check=0

    for (( i=0; i<${#pass}; i++ )); do
        for (( j=0; j<${#cap}; j++ )); do
            if [ "${pass:$i:1}" = "${cap:$j:1}" ];then
                check=1
                break
            fi
        done
    done
    if [ $check -eq 0 ];then
        return 0
    fi
    for (( i=0; i<${#pass}; i++ )); do
        for (( j=0; j<${#small}; j++ )); do
            if [ "${pass:$i:1}" = "${small:$j:1}" ];then
                check=2
                return 1;
            fi
        done
    done

    if [ $check -eq 1 ];then
        return 0
    fi
}
```
- Memiliki alpha numeric.
Masalah ini dapat diselesaikan dengan mengevaluasi password (apakah mengandung angka atau tidak) **setelah** evaluasi huruf besar dan huruf kecil. Hanya bermain di `if else` syntax

```

	not_Alnum () {
    num="1234567890"
    pass=$1
    check=0

    for (( i=0; i<${#pass}; i++ )); do
        for (( j=0; j<${#num}; j++ )); do
            if [ "${pass:$i:1}" = "${num:$j:1}" ];then
                check=1
                return 1
            fi
        done
    done
    if [ $check -eq 0 ];then
        return 0
    fi
    }
```
```
	elif not_Alnum $password
    then
        echo "password haruslah alphanumeric"

```
- Tidak boleh sama dengan username.
Masalah ini diselesaikan dengan fungsi `awk`. Pertama obtain dahulu list username dari file user.txt dan masukkan ke dalam suatu variable, lalu lakukan tracing linear password terhadap list user tadi, kasus selesai.
```
    ada_di_nama () {
    inp=$1
    list_user=($(awk '{print $1}' user.txt))
    for(( i=0; i<${#list_user[@]}; i++ ));do
        if [ "$inp" = "${list_user[$i]}" ];then
            return 0
        fi
    done
    return 1
}
.....
    if ada_di_nama $username
    then
        echo "$date_time REGISTER: ERROR User already exists" >> log.txt
        echo "$date_time REGISTER: ERROR User already exists"
....

```

### 1C
- Setiap percobaan akan tercatat pada log.txt dengan format: MM/DD/YY hh:mm:ss **MESSAGE**. Message sesuai dengan yang ada di soal. Untuk mendeteksi apakah password yang diberikan user benar atau salah, cukup dengan menggunakan fungsi `awk` untuk obtain list user dan list password, lalu lakukan traverse pada user (cocokan dengan input user). Jika sudah, ambil indexnya, dan obtain password user **pada list password**, lalu cocokan dengan password input.

**FUNGSI MENCOCOKAN USERNAME DAN PASSWORD**
```
    isnot_match(){
	user_inp=$1
	pass_inp=$2
	check=0
	index=-1
	usernames=($(awk '{print $1}' user.txt))
	password=($(awk '{print $2}' user.txt))

	for(( i=0; i<${#usernames[@]}; i++ ));do
		if [ "$user_inp" = "${usernames[$i]}" ];then
			check=1
			index=i
			break
		fi
	done

	if [ $check -eq 0 ];then
		return 0
	fi

	if [ "$pass_inp" = "${password[$index]}" ];then
		return 1
	else
		return 0
	fi
    }
```

**MEMASUKKAN MESSAGE UNTUK REGISTER**
```
....
    read -p  "Enter username: " username
	read -s -p  "Enter password: " password
	date_time=$(date +"%m/%d/%y %T")

	len=${#password}
	if ada_di_nama $username
	then
		echo "$date_time REGISTER: ERROR User already exists" >> log.txt
....
    echo "$date_time INFO User $username registered successfully" >> log.txt
    echo "$date_time INFO User $username registered successfully"
```
**MEMASUKKAN MESSAGE UNTUK LOGIN**
```
....
case $m in
		"Login")
		date_time=$(date +"%m/%d/%y %T")
			read -p "Enter username: " username
			read -s -p "Enter password: " password
			new=$password
			if isnot_match $username $password;then
				echo ""
				echo "$date_time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
				echo "$date_time LOGIN: ERROR Failed login attempt on user $username"
			else
				echo ""
				echo "$date_time LOGIN: INFO User $username logged in" >> log.txt
				echo "$date_time LOGIN: INFO User $username logged in"
				after_login "$username" "$new";
			fi
....
```
### 1D
- Diberi 2 buah command, yaitu **dl N** untuk mendownload file sebanyak N dari website https://loremflickr.com/320/240, dan **att** untuk menghitung jumlah login.

**LOGIKA COMMAND dl N**

Kasus ini mewajibkan kita untuk menaruh file yang kita download dengan format **YYYY-MM-DD_USERNAME** dan file yang di download harus dalam format **PIC_XX** dimana XX adalah urutan nomor.
- Untuk masalah format folder, cukup dengan membuat variabel sesuai dengan format yang di concatinate dengan username saat itu.
- Untuk perilah download, disini kami menggunakan fungsi `wget` kemudian nama output filenya disesuaikan dengan jumlah file yang didownload. Namun, di sistem perlu mengetahui apakah folder sudah ada di direktori atau tidak dan berapa jumlah file yang ada di di folder apabila folder tersebut ada sehingga perlu dilakukan pengkondisian. Kodenya sebagai berikut:
```
    if [[ $command == *"dl"* ]];then
			#obtain the N value
			N=${command:3}

			new_folder=$(date +"%Y-%m-%d")_$user_inp
			if test -f "$new_folder.zip";then
				unzip -q -P "$pass_inp" -q "$new_folder.zip"
			else
				#create directory if it doesn't exist and change directory
				mkdir -p $new_folder
			fi

			# direktori ke new folder
			cd $new_folder

			#count the number of files
			amt_files=$(($(find . -type f | wc -l)-0))

			#update the values of N
			N=$N+$amt_files
			#download
			for (( i=$amt_files+1; i<=$N; i++ ))
			do
				if [ $i -lt 10 ];then
					name="PIC_0$i"
				else
					name="PIC_$i"
				fi
			wget  https://loremflickr.com/320/240 -O $name
			done
```
1. Jika folder zip sudah tersedia, maka kita harus unzip.
```
    unzip -q -P "$pass_inp" -q "$new_folder.zip"
```
2. Jika tidak, buatlah direktori sesuai format
```
mkdir -p $new_folder
```
3. Pindahkan ke direktori folder dengan syntax `cd $newfolder` dan lakukan proses penghitungan file pada folder tersebut dengan syntax `amt_files=$(($(find . -type f | wc -l)-0))`.

4. Jika sudah, lakukan proses download dengan `for loop` sebanyak N sesuai dengan jumlah file yang ada di folder. Misal apabila file di dalam folder 0, maka format file download akan **PIC_01, PIC_02, PIC_03, dst**. Jika, katakanlah, file di folder 6, maka format file download akan **PIC_07, PIC_08, PIC_09, dst**

- Masalah zip. Masalah zip ini cukup dengan kode:
```
	zip -q -P "$pass_inp" -r "$new_folder.zip" "$new_folder"
```

**LOGIKA UNTUK COMMAND ATT**

- Kasus ini cukup diselesaikan dengan fungis `awk` dan ambil jumlah fail serta successnya sehingga total login adalah jumlah fail ditambah jumlah sukses.
```
elif [ "$command" = "att" ];then
			pat1="LOGIN: INFO User $user_inp"
			pat2="LOGIN: ERROR Failed login attempt on user $user_inp"
			success=`awk -v key="$pat1" '$0~key{print}' log.txt | wc -l`
			fail=`awk -v key_2="$pat2" '$0~key_2{print}' log.txt | wc -l`
			att_count=$(($success + $fail))

			echo $att_count
```

### HASIL NO 1 KESELURUHAN

**LOG dan USER .txt**

![](images_soal1/log.png)
![](images_soal1/user.png)

**KETENTUAN PASSWORDD**

![](images_soal1/8char.png)
![](images_soal1/alphanumeric.png)
![](images_soal1/hurufkecilbesar.png)
![](images_soal1/passequsername.png)
![](images_soal1/usersudahada.png)

**ZIP DOWNLOAD**

![](images_soal1/zipdownload.png)

## Nomer 2
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut

inisiasi working space kedalam beberapa variebel untuk mempermudah dalam menulisakan code
```
locfolder=/home/ilhamfz/Sisop/Modul1
loclog=$locfolder/log_website_daffainfo.log
locpwd=$locfolder/forensic_log_website_daffainfo_log
```


### 2A
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
```
mkdir $locpwd
...
```
Buat folder jika belum tersedia, jika sudah ada, hapus folder tersebut dengan `rm -r $locpwd` lalu buat ulang foldernya dengan code diatas


### 2B
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
```
...
awk '
END {print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "request per jam"}' $loclog>$locpwd/ratarata.txt
...
```
Gunakan `NR` untuk mengetahui jumlah line yang ada dalam file log. `(NR-1)/12` kurangkan 1 karena pada line pertama file log bukan merupakan request, lalu dibagi 12 untuk menghitung rata-rata per jamnya karena dalam file log hanya ditampilkan dari jam 00.00 hingga jam 12.00.
Hasil tersebut lalu di print kedalam file `ratarata.txt` dalam working space `$locpwd`


### 2C
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak
melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
```
awk -F\" ' {print $2}' $loclog | sort | uniq -c | sort -rn | head -n1 | awk '
{print "IP yang paling banyakmengakses server adalah:", $2, "sebanyak", $1, "request"}' >$locpwd/result.txt
```
Dalam code tersebut digunakan dua awk sekaligus. Awk pertama digunakan untuk menampilkan seluruh ip dengan menghilangkan ip yang duplicate dengan syntax `uniq` dan dihitung masing-masing ip tersebut oleh syntax `-c` dan di sorting dengan urutan jumlah ip tersebut, namun hanya ditampilkan baris pertama dari daftar ip dan jumlahnya untuk mendapatkan ip yang melakukan request paling banyak. Awk kedua digunakan untuk memisahkan data ip dan data jumlah ip request dari ip tersebut, lalu dilakukannya print kedalam file `result.txt` pada working space `$locpwd`


### 2D
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
```
awk -F\" ' /curl/ {++n} END {print "Ada", n, "request yang menggunakan curl sebagai user-agent"}' $loclog>>$locpwd/result.txt
```
Dalam awk tersebut, digunakan pencarian kata `curl` dalam setiap line data log, dan dicatata jumlahnya dalam variabel `n`, lalu dilakukannya print kedalam file `result.txt` pada working space `$locpwd`


### 2E
Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
```
awk -F\" ' /2022:02/ {print $2}' $loclog | uniq >> $locpwd/result.txt
```
Digunakan pula awk untuk menampilkan data dari file log yang memiliki kecocokan dengan `2022:02` dan tampilkan setiap ip dari data yang telah diperoleh. Gunakan syntax `uniq` untuk menghilangkan data ip yang duplicate, lalu dilakukannya print kedalam file `result.txt` pada working space `$locpwd`


### 2 Hasil
![Nomer_2_Hasil](/uploads/93f466df07eb35845cd4ff4c130be4dc/Nomer_2_Hasil.png)

Setelah file `soal2_forensic_dapos.sh` dijalankan, ditemukannya folder `forensic_log_website_daffainfo_log` dalam direktory `$locfolder`. Dalam folder tersebut juga ditemukan file `ratarata.txt` dan `result.txt` berisikan data seperti gambar diatas.



## Nomer 3

### 3A
![Nomer_3_Hasil](https://i.imgur.com/fFlzDJb.png)

Monitoring memory dan disk di pc dengan target path yang akan dimonitoring yaitu $HOME lalu akan menampilkan nilai dari ram dan size pada target  path tersebut menggunakan perintah echo.
Variable log_date untuk menyimpan format nama dari file nantinya sesuai dengan format metrics_{YmdHms}.log.
```
...
#! /bin/bash 
NOW=$(date +"%Y%m%d%H%M%S")
mkdir -p "$HOME/log/"
echo "Minute To Log ..."
exec 3>&1 4>&2 >>$HOME/log/"metrics_$NOW.log" && chmod 700 $HOME/log/"metrics_"$NOW.log
printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n"		
M=$(free -m | awk 'NR==2{printf $2","$3","$4","$5","$6","$7} NR==3{printf ","$2","$3","$4","}') 				
D=$(du -s -m $HOME/ | sed 's/^\([0-9]*\)\(.*\)$/\1M \2/'| awk '{printf $2","$1}')								
echo "$M$D$S" 
...
```
### 3B
Karena script minute_log.sh yang diminta harus berjalan otomatis pada tiap menit. Masukkan perintah crontab -e untuk membuat crontab baru, lalu pada bagian bawah script crontab, kita tulis * * * * * bash /directory script/minute_log.sh
### 3C
Membuat script shell dengan nama aggregate_minutes_to_hourly_log.sh. Dan menyortir File yang di ubah dalam kurun waktu 60 menit agar semua file log dari minute_log.sh yang sudah di generate dalam jangka waktu 60 Menit bisa dibaca dan di exsekusi oleh script aggregate_minutes_to_hourly_log.sh

```
...
find $LOG/ -type f -name 'metrics_2*.log' -amin -60	-exec cat {} + >> $LOG/output.txt 
...
```
Menggunakan Awk Untuk menyortir baris dari log yang akan diambil(hanya baris yang berada di baris Genap yang akan di ambil)
```
...
awk '(FNR%2 == 0)' $LOG/output.txt > $LOG/result.txt 
...
```
Menghapus Patch , dan patch size untuk meng exsekusi MIN MAX AVG (dikarenakan variabel path ($HOME) hanya berbentuk huruf dan path size akan di exsekusi secara terpisah di line lain
```
...
awk '{sub(/\/home.*$/,"",$1)}1' $LOG/result.txt > $LOG/output1.txt
...
```
menyortir value dan menghapus di bagian memory && swap (generate untuk memory) untuk diesksekusi secara terpisah (untuk bagian path_size)
```
...
sed 's/.$//' $LOG/output1.txt > $LOG/output.txt
sed 's/.*,\(,*\)/\1/' $LOG/result.txt > $LOG/patch.txt
...
```
Mencari Nilai Max Min dan AVG untuk semua memory dan swap 
```
...
MIN=$(awk 'NR == 1 || $3 < min {minim = $0; min = $3}END{print "minimum,"  minim}' $LOG/output1.txt)
MAX=$(awk -F'#' -v max=0 '{if($1>max){maxim=$0; max=$1}}END{print "maximum," maxim}' $LOG/output1.txt)
AVG=$(awk -F, '{for(i=1; i<=NF; i++) {a[i]+=$i; if($i!="") b[i]++}}; END {for(i=1; i<=NF; i++) printf "%s ,%s", a[i]/b[i], (i==NF?ORS:OFS)}' $LOG/output.txt)
...
```
Mencari Nilai Max Min dan AVG dari Path size
```
...
MAX1=$(awk -F idx=1 'NR==1 || $idx>max{max=$idx} END{print max}' $LOG/patch.txt)
MIN1=$(awk 'min=="" || $1 < min {min=$1} END {printf min}' $LOG/patch.txt)
AVG1=$(awk '{sum+=$1} END {printf sum/NR}' $LOG/patch.txt)
...
```
![Nomer_3_Hasil](https://i.imgur.com/8ZVz78n.png)
### 3D
Agar semua file log hanya dapat dibaca oleh user , kita dapat menjalankan perintah chmod 700 $output

Perintah chmod untuk mengubah perizinan user
Angka 7 agar user dapat melakukan read, write, dan execute pada file
Angka 0 kedua agar grup user tidak memiliki akses terhadap file
Angka 0 ketiga agar orang lain tidak memiliki akses terhadap file



</div>
