
#! /bin/bash 


NOW=$(date +"%Y%m%d%H%M%S")
echo "Program Hours..."
 find . -type f -name 'metrics_2*.log' -amin -60 -exec cat {} + >> output.txt

awk '(FNR%2 == 0)' $HOME/log/output.txt > result.txt 

printf "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size \n"	
#free -m | awk ' NR==1{printf"mem_"$1",mem_"$2",mem_"$3",mem_"$4",mem_"$5",mem_"$6",swap_"$1",swap_"$2",swap_"$3, path,path_size}') 		#string named(auto)
MIN=$(awk 'NR == 1 || $3 < min {line = $0; min = $3}END{printf "\nminimum,"  line}' result.txt)
MAX=$(awk -F'#' -v max=0 '{if($1>max){content=$0; max=$1}}END{printf "\nmaximum," content}' result.txt)


#exec 3>&1 4>&2 >> $HOME/log/"metrics_agg_$NOW".log			
echo "$MIN$MAX" 
 [ -e result.txt ] && rm result.txt
 [ -e  output.txt ] && rm  output.txt


