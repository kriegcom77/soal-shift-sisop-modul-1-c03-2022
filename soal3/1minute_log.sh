#! /bin/bash 
NOW=$(date +"%Y%m%d%H%M%S")
mkdir -p "$HOME/log/"
echo "Program Starting..."
exec 3>&1 4>&2 >>$HOME/log/"metrics_$NOW.log"
printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n"		
M=$(free -m | awk 'NR==2{printf $2","$3","$4","$5","$6","$7} NR==3{printf ","$2","$3","$4","}') 				
D=$(du -sh $HOME/| awk '{printf $2","$1}')								
echo "$M$D$S" 


