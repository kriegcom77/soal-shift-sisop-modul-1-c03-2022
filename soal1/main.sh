#!/bin/bash
isnot_match(){
	user_inp=$1
	pass_inp=$2
	check=0
	index=-1
	usernames=($(awk '{print $1}' user.txt))
	password=($(awk '{print $2}' user.txt))

	for(( i=0; i<${#usernames[@]}; i++ ));do
		if [ "$user_inp" = "${usernames[$i]}" ];then
			check=1
			index=i
			break
		fi
	done

	if [ $check -eq 0 ];then
		return 0
	fi

	if [ "$pass_inp" = "${password[$index]}" ];then
		return 1
	else
		return 0
	fi
}

after_login () {
	on=1
	while [ $on -eq 1 ]
	do
		user_inp=$1
		pass_inp=$2
		echo "Pilih menu di bawah:
		1) dl N (download file)
		2) att (menghitung percobaan login)
		3) log out"

		read -p "Enter command: " command
		# cek command
		if [[ $command == *"dl"* ]];then
			#obtain the N value
			N=${command:3}

			new_folder=$(date +"%Y-%m-%d")_$user_inp
			if test -f "$new_folder.zip";then
				unzip -q -P "$pass_inp" -q "$new_folder.zip"
			else
				#create directory if it doesn't exist and change directory
				mkdir -p $new_folder
			fi

			# direktori ke new folder
			cd $new_folder

			#count the number of files
			amt_files=$(($(find . -type f | wc -l)-1))

			#update the values of N
			N=$N+$amt_files
			#download
			for (( i=$amt_files+1; i<=$N; i++ ))
			do
				if [ $i -lt 10 ];then
					name="PIC_0$i"
				else
					name="PIC_$i"
				fi
			wget  https://loremflickr.com/320/240 -O $name
			done

			#ubah direktori ke direktori sebelum
			cd -

			#zip
			zip -q -P "$pass_inp" -r "$new_folder.zip" "$new_folder"

			#remove folder
			rm -r $new_folder
		elif [ "$command" = "att" ];then
			pat1="LOGIN: INFO User $user_inp"
			pat2="LOGIN: ERROR Failed login attempt on user $user_inp"
			success=`awk -v key="$pat1" '$0~key{print}' log.txt | wc -l`
			fail=`awk -v key_2="$pat2" '$0~key_2{print}' log.txt | wc -l`
			att_count=$(($success + $fail))

			echo $att_count
		elif [ "$command" = "log out" ];then
			on=0
		else
			echo "invalid input"
		fi
	done
}

menu=("Login" "Register" "Exit")
echo "Selamat datang di komputer Han, silahkan pilih menu dibawah:"

select m in "${menu[@]}"
do
	case $m in
		"Login")
		date_time=$(date +"%m/%d/%y %T")
			read -p "Enter username: " username
			read -s -p "Enter password: " password
			if isnot_match $username $password;then
				echo ""
				echo "$date_time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
				echo "$date_time LOGIN: ERROR Failed login attempt on user $username"
			else
				echo ""
				echo "$date_time LOGIN: INFO User $username logged in" >> log.txt
				echo "$date_time LOGIN: INFO User $username logged in"
				after_login $username $password
			fi

		;;
		"Register")
			bash register.sh
		;;
		"Exit")
			exit 0
			break
		;;
		*)
			echo "Invalid input"
	esac
done
		

  
