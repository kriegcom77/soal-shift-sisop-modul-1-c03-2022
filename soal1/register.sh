#!/bin/bash

echo "Sebelum menggunakan komputer Han, silahkan register terlebih dahulu!"
ada_di_nama () {
	inp=$1
	list_user=($(awk '{print $1}' user.txt))
	for(( i=0; i<${#list_user[@]}; i++ ));do
		if [ "$inp" = "${list_user[$i]}" ];then
			return 0
		fi
	done
	return 1
}
not_Big () {
	cap="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	small="abcdefghijklmnopqrstuvwxyz"
	pass=$1
	check=0

	for (( i=0; i<${#pass}; i++ )); do
		for (( j=0; j<${#cap}; j++ )); do
			if [ "${pass:$i:1}" = "${cap:$j:1}" ];then
				check=1
				break
			fi
		done
	done
	if [ $check -eq 0 ];then
		return 0
	fi
	for (( i=0; i<${#pass}; i++ )); do
		for (( j=0; j<${#small}; j++ )); do
			if [ "${pass:$i:1}" = "${small:$j:1}" ];then
				check=2
				return 1;
			fi
		done
	done

	if [ $check -eq 1 ];then
		return 0
	fi
}

not_Alnum () {
	num="1234567890"
	pass=$1
	check=0

	for (( i=0; i<${#pass}; i++ )); do
		for (( j=0; j<${#num}; j++ )); do
			if [ "${pass:$i:1}" = "${num:$j:1}" ];then
				check=1
				return 1
			fi
		done
	done
	if [ $check -eq 0 ];then
		return 0
	fi
}


flag=1

while [ $flag == 1 ]
do
	read -p  "Enter username: " username
	read -s -p  "Enter password: " password
	date_time=$(date +"%m/%d/%y %T")

	len=${#password}
	if ada_di_nama $username
	then
		echo "$date_time REGISTER: ERROR User already exists" >> log.txt
		echo "$date_time REGISTER: ERROR User already exists"
	elif [ "$username" = "$password" ]
	then
		echo "password tidak boleh sama dengan username"
	elif [ $len -lt 8 ]
	then
		echo "password harus lebih dari 8 karakter"
	elif not_Big $password
	then
		echo "password harus mengandung minimal 1 huruf kecil dan 1 huruf besar"
	elif not_Alnum $password
	then
		echo "password haruslah alphanumeric"
	else
		flag=0
	fi
done
echo "$username $password" >> user.txt
echo "$date_time INFO User $username registered successfully" >> log.txt
echo "$date_time INFO User $username registered successfully"

