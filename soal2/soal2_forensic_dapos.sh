#!/bin/bash
locfolder=/home/ilhamfz/Sisop/Modul1
loclog=$locfolder/log_website_daffainfo.log
locpwd=$locfolder/forensic_log_website_daffainfo_log

#A
if [[ -d "$locpwd" ]]
then
	rm -r $locpwd
	mkdir $locpwd
else
	mkdir $locpwd
fi

#B
awk '
END {print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "request per jam"}' $loclog>$locpwd/ratarata.txt

#C
awk -F\" ' {print $2}' $loclog | sort | uniq -c | sort -rn | head -n1 | awk '
{print "IP yang paling banyakmengakses server adalah:", $2, "sebanyak", $1, "request"}' >$locpwd/result.txt

#D
printf "\n" >> $locpwd/result.txt
awk -F\" ' /curl/ {++n} END {print "Ada", n, "request yang menggunakan curl sebagai user-agent"}' $loclog>>$locpwd/result.txt

#E
printf "\n" >> $locpwd/result.txt
awk -F\" ' /2022:02/ {print $2}' $loclog | uniq >> $locpwd/result.txt
